// class Person {
//     name: string;
//     age: number;
//     address: string;

//     constructor(name: string, age: number, address: string) {
//         this.name = name;
//         this.age = age;
//         this.address = address;
//     }

//     makeSleep(hours: number): string {
//         return `this ${this.name} will sleep for ${hours}`
//     }
// }

// class Student extends Person {


//     constructor(name: string, age: number, address: string) {
//         super(name, age, address);
//     }
// }

// const student1 = new Student('shoumo', 16, 'Dhaka');
// student1.makeSleep(8);
// console.log(student1);



// class Teacher extends Person {
//     designation: string;

//     constructor(name: string, age: number, address: string, designation: string) {
//         super(name, age, address)
//         this.designation = designation;
//     }
//     takeClasses(numOfClass: number): string {
//         return `this ${this.name} will take ${numOfClass}`
//     }
// }

// const teacher1 = new Teacher('sampa', 30, 'France', 'professor');
// teacher1.takeClasses(5);
// console.log(teacher1);

