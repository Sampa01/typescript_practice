// npx ts-node-dev --respawn ./src/module4/practice.ts

//1. Convert the following JavaScript array into a TypeScript tuple
// const userInfo = [101, "Ema", "John", true,  , "2023"];

// const userInfo: [number, string, string, boolean, undefined, string] = [101, "Ema", "John", true, , "2023"];
// console.log(userInfo);

//2. Write a TypeScript function that takes in two arrays of numbers as parameters.
//  The function should compare the elements in both arrays
//  and return a new array that contains only the elements that are present in both arrays.


// function compare(num1: number[], num2: number[]): number[] {
//     const commonArr: number[] = [];
//     for (const arr of num1) {
//         if (num2.includes(arr)) {
//             commonArr.push(arr);
//         }
//     }

//     return commonArr;
// };
// const num1: number[] = [1, 2, 3, 4, 5];
// const num2: number[] = [3, 4, 5, 6, 7];

// const commonArr: number[] = compare(num1, num2);
// console.log(commonArr);

// 3.You have an interface for Product, containing the product's id, name, price, and category. 
// You want to filter an array of Products based on a specific criterion and value.


// interface Product {
//     id: number;
//     name: string;
//     price: number;
//     category: string;
// }

// const products: Product[] = [
//     { id: 1, name: "Product 1", price: 10, category: "Category A" },
//     { id: 2, name: "Product 2", price: 20, category: "Category B" },
//     { id: 3, name: "Product 3", price: 30, category: "Category A" },
//     { id: 4, name: "Product 4", price: 40, category: "Category B" },
//     { id: 5, name: "Product 5", price: 50, category: "Category A" }
// ];

// function filterProduct(products: Product[], criterion: keyof Product, value: any): Product[] {
//     return products.filter(product => product[criterion] === value);
// }
// const filteredProducts = filterProduct(products, "category", "Category B");
// console.log(filteredProducts);

// 4.Suppose you have an array of tuples, where each tuple represents a product and 
// contains the product name, price, and quantity. Write a TypeScript function that calculates the total cost of all the 
// products in the array, using a generic type for the tuple and a type alias for the array.

// type ProductTuple = [string, number, number];
// type ProductArray = ProductTuple[];

// function getTotalCost(products: ProductArray): number {
//     let totalCost = 0;
//     for (const product of products) {
//         const [name, price, quantity] = product;
//         totalCost += price * quantity;

//     }
//     return totalCost;
// }

// const products: ProductArray = [
//     ["Product 1", 10, 2],
//     ["Product 2", 20, 1],
//     ["Product 3", 30, 3],
// ];

// const totalCost: number = getTotalCost(products);
// console.log(totalCost);


// Suppose you have an array of numbers in TypeScript, and you want to find the sum of all the even numbers in the array.
//  How would you approach this problem and write code to solve it?



// function addEvenNum(numbers: number[]): number {
//     let sum = 0;
//     for (const number of numbers) {
//         if (number % 2 === 0) {
//             sum += number;

//         }
//     }
//     return sum;
// }

// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// const sum = addEvenNum(numbers);
// console.log(sum);

// Create an interface called Person that includes properties for name (string), age (number), and email (string). 
// Then create an array of Person objects and write a function that takes the array and a string email as parameters,
//  and returns the Person object that matches the email or null if no match is found.

// interface Person {
//     name: string;
//     age: number;
//     email: string;
// }

// const persons: Person[] = [
//     { name: "sampa", age: 30, email: "ssi" },
//     { name: "sandra", age: 10, email: "ssianna" },
//     { name: "saman", age: 3, email: "sitoll" },
//     { name: "stitoi", age: 20, email: "ssmljhfss" },
// ]

// function findPersonByEmail(persons: Person[], email: string): Person | null {
//     for (const person of persons) {
//         if (person.email === email) {
//             return person;
//         }
//     }
//     return null;
// }

// const email = 'sitoll';
// const people = findPersonByEmail(persons, email);

// if (people) {
//     console.log(people.name); // Output: Bob Smith
// } else {
//     console.log(`No person found with email ${email}`);
// }


// 7.Create a TypeScript program that declares an array of numbers. 
// Use the spread  operator to pass the elements of the array as arguments to a function that finds the minimum and 
// maximum values of the array. 
// Use destructuring to assign the minimum and maximum values to separate variables, and log them to the console.
// const numbers = [2, 5, 90, 56, 99];

// function findMinMaxNum(...numbers: number[]): [number, number] {
//     const min = Math.min(...numbers);
//     const max = Math.max(...numbers);
//     return [min, max];
// }
// const [min, max] = findMinMaxNum(...numbers);
// console.log(`minimum num is ${min}`);
// console.log(`minimum num is ${max}`);

// Create a TypeScript program that declares a function that takes a string parameter with a literal 
// type of "red", "green", or "blue", and an optional boolean parameter. If the boolean parameter is true, log the string parameter 
// in uppercase.If the boolean parameter is false or not provided, log the string parameter in lowercase.



function logColor(color: "red" | "green" | "blue", uppercase?: boolean) {
    if (uppercase) {
        console.log(color.toUpperCase());
    } else {
        color.toLowerCase();
    }
}
logColor("red"); // "red"
logColor("green", true); // "GREEN"
logColor("blue", true); // "blue"

