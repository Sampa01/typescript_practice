// // of guard

// import { type } from "os";

// // type AphaNum = string | number;

// // function add(param1: AphaNum, param2: AphaNum): AphaNum {
// //     if (typeof param1 == "number" && typeof param2 === "number") {
// //         return param1 + param2;
// //     } else {
// //         return param1.toString() + param2.toString();
// //     }
// // }
// // add('1', '2');
// // add(1, 3);
// // console.log(add(2, 3));

// // in guard

// type NormalUserType = {
//     name: string;
// };

// type AdminUserType = {
//     name: string;
//     role: 'admin';
// }

// function getUser(user: NormalUserType | AdminUserType) {
//     if ('role' in user) {
//         return `I am a admin and my role is ${user.role}`
//     }
//     else {
//         return `I am a normal user`
//     }
// }

// const NormalUser1: NormalUserType = { name: 'Mr DODO' };
// const AdminUser1: AdminUserType = { name: 'Mou', role: "admin" };

// console.log(getUser(NormalUser1));
// console.log(getUser(AdminUser1));

// // instanceof guard




